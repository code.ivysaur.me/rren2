# rren2

![](https://img.shields.io/badge/written%20in-PHP-blue)

A command-line bulk renaming utility.

This is a cross-platform successor to the [rren](https://code.ivysaur.me/rren/) project, with largely the same usage parameters, because the original was written against obsolete TR1 libraries and against windows-specific APIs.

## Usage


```
Usage:
   rren -e|-r MASK FIND REPLACE

Flags:
   -e      Treat FIND and REPLACE as regular expressions.
           This version of rren uses the PCRE engine.
   -r      Treat FIND and REPLACE as raw strings.
   --help  Display this message
```


## Changelog

2017-04-23
- Initial public release

2017-04-05 2.0
- Initial private release
- [⬇️ rren-2.0.tar.xz](dist-archive/rren-2.0.tar.xz) *(1.52 KiB)*


2017-04-04
- Project began
